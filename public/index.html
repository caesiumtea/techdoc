<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Documentation for Google Sheets Spellcheck Function</title>
</head>

<body>
    <nav id="navbar">
        <div id="nav-wrapper">
            <header>
                <h1 id="title">Documentation for Google Sheets Spellcheck Function</h1>
            </header>
            <ul>
                <li><a class="nav-link" href="#Introduction">Introduction</a></li>
                <li><a class="nav-link" href="#Purpose">Purpose</a></li>
                <li><a class="nav-link" href="#Usage">Usage</a></li>
                <li><a class="nav-link" href="#How_it_works">How it works</a></li>
                <li><a class="nav-link" href="#Modifying_the_behavior">Modifying the behavior</a></li>
                <li><a class="nav-link" href="#Further_development">Further development</a></li>
                <li><a class="nav-link" href="#License">License</a></li>
                <li><a class="nav-link" href="#Source_code">Source code</a></li>

            </ul>
        </div>
    </nav>

    <main id="main-doc">

        <section class="main-section" id="Introduction">
            <header>
                <h2>Introduction</h2>
            </header>
            <p>This is a custom function that allows you to passively check the spelling of individual cells in Google
                Sheets,
                using <a href="https://developers.google.com/apps-script">Google Apps Script</a>. To use this function
                in
                your
                spreadsheet, paste the script into that spreadsheet's Google Apps Script settings, then access it with
                the
                function name <code>SPCHECK</code>.</p>
            <p><a href="https://github.com/caesiumtea/sheets-spellcheck">View the script's source code on GitHub</a> or
                <a href="#Source_code">below</a>.
            </p>

            <aside>
                <strong>Warning:</strong> This is a very hacky solution, which relies on making HTTP requests to a dictionary website.
                    Which, idk, might not be great netiquette. It is not intended for long-term use or for applying to very large spreadsheets.
            </aside>
        </section>

        <section id="Purpose" class="main-section">
            <header>
                <h2>Purpose</h2>
            </header>
            <p>The spell check function built into Google Sheets can only be used interactively, stepping through the
                document
                one error at a time, and is intended for when you want to <em>correct</em> spelling errors. However,
                sometimes
                you just need to know whether a typo <em>exists</em> in a cell without wanting to change it, and
                sometimes
                you
                might need that information to be visible while you're typing in other cells. </p>
            <p>For example, this script may be useful for making a list of misspellings to add to an autocorrect list,
                or if
                you
                have a spreadsheet that collects text submitted by others, and you want to identify submissions with
                typos
                so
                that you can contact the writer for clarification. </p>
        </section>

        <section id="Usage" class="main-section">
            <header>
                <h2>Usage</h2>
            </header>
            <p>The following instructions are based on the desktop browser version of Google Sheets.</p>

            <h3 id="installation">Installation</h3>
            <p>The script must be added to each spreadsheet where you want to use it. </p>
            <ol>
                <li>Open your spreadsheet in Google Sheets.</li>
                <li>In the menu, select <code>Extensions &gt; Apps Script</code>.</li>
                <li>You should see a text editor with a file named <code>Code.gs</code>. Delete any existing text in
                    <code>Code.gs</code>.
                </li>
                <li>Select the entire script text, paste it into <code>Code.gs</code>, and save. You should not need to
                    select Deploy.</li>
            </ol>
            <p>You should now have a new function named <code>SPCHECK</code> available in your spreadsheet. </p>
            <p>For more details about adding Apps Script or custom functions to Google Sheets, see the <a
                    href="https://developers.google.com/apps-script/quickstart/custom-functions">Apps Script
                    documentation</a>.
            </p>

            <h3 id="using-the-function-in-a-spreadsheet">Using the function in a spreadsheet</h3>
            <p>Usage is mostly the same as any other Sheets function. </p>
            <ol>
                <li>Select a blank cell in the spreadsheet and type<code>=SPCHECK</code>. It may appear in the list of
                    suggestions once you start typing.
                    <ul>
                        <li>Currently, the function does <em>not</em> seem to appear in the list of functions under
                            the<code>Insert</code> menu, so you'll need to type it out.</li>
                        <li>Be sure to include the <code>=</code> at the beginning, as you would for any other Sheets
                            function.
                        </li>
                    </ul>
                </li>
                <li>Select a cell, or range of cells, as input to be spell-checked.</li>
                <li>The output of the function will appear in the cell where you entered the function, or in the
                    adjacent
                    cells
                    if you selected a range as input.</li>
            </ol>

            <h4 id="output">Output</h4>
            <ul>
                <li>Each misspelling from the input will be printed in the output, separated by a space.</li>
                <li>A blank output cell indicates that the function did not find any misspelled words in the input cell.
                    <ul>
                        <li>If you prefer to see some text indicating a successful check with no errors found, see <a
                                href="#modifying-the-behavior">Modifying the behavior</a> below. </li>
                    </ul>
                </li>
            </ul>

            <h3 id="troubleshooting">Troubleshooting</h3>

            <h4 id="-name-">#NAME?</h4>
            <p>If the cell where you entered the function displays the text <code>#NAME?</code>, this means Google
                Sheets
                cannot
                find the definition of the function. Check that you pasted the script correctly into
                <code>Code.gs</code>.
                If
                the script was entered correctly, you might also try refreshing the spreadsheet after saving the script.
            </p>

            <h4 id="too-many-requests">Too many requests</h4>
            <p>This function does not have its own internal dictionary, and instead checks spelling by looking up your
                text
                in
                an external online dictionary. If you use the spell check function on too much text too quickly, it is
                possible
                for the dictionary service to stop responding to these requests. I am not sure what the limit is. Note
                that
                one
                request is sent for each <em>word</em> of input, not for each cell.</p>

            <h4 id="internet-connection">Internet connection</h4>
            <p>This function will not work offline. An internet connection is required in order to access the
                dictionary.
            </p>
        </section>

        <section id="How_it_works" class="main-section">
            <header>
                <h2>How it works</h2>
            </header>
            <p>The script has two parts: the <code>SPCHECK</code> function, which gets called from Google Sheets, and
                the
                helper
                function <code>hasTypo_</code>, which the user cannot access directly. <code>hasTypo_</code> contains
                the
                main
                logic of the script and takes a single cell as input, while the body of <code>SPCHECK</code> handles
                ranges
                as
                input, and calls <code>hasTypo_</code> on each individual cell.</p>

            <h3 id="inputs">Inputs</h3>
            <p>The input may either be a string that represents the content of a single cell of a spreadsheet, or an
                array
                of
                strings representing the contents from a range of adjacent cells.</p>

            <h3 id="outputs">Outputs</h3>
            <p>Output is rendered as text in the Sheets cell where the user calls the <code>SPCHECK</code> function.
                There
                are
                two possible outputs:</p>
            <ul>
                <li>If the spell check finds no misspelled words, then the output is an empty string.</li>
                <li>Otherwise, the output is a string that contains a sequence of all the misspelled words from the
                    input,
                    separated by spaces.<br>Note that "misspelled word" means any word which was not found in the
                    dictionary reference.</li>
            </ul>

            <h3 id="steps">Algorithm</h3>
            <ul>
                <li>Check whether the input is an array or a single cell</li>
                <li>If the input is an array, use a mapping function to call <code>hasTypo_</code> on each cell ad
                    return
                    the
                    result as a new array of cells</li>
                <li>If input is not an array (which means it must be a single cell), directly call <code>hasTypo_</code>
                    on
                    the
                    input</li>
            </ul>

            <h4 id="tokenization">Tokenization</h4>
            <ul>
                <li>Read the cell as a string</li>
                <li>Split the string into an array of words, based on the regex <code>/[^a-zA-Z']+/</code></li>
            </ul>

            <h4 id="dictionary-http-request">Dictionary HTTP request</h4>
            <ul>
                <li>For each word, try accessing <code>https://www.dictionary.com/browse/</code> plus the word</li>
                <li>If accessing that URL generates response code 404, then this means the word is not in the
                    dictionary, so
                    add
                    the word to a list of typos</li>
                <li>Return that list and print it to the output cell</li>
            </ul>
        </section>

        <section id="Modifying_the_behavior" class="main-section">
            <header>
                <h2>Modifying the behavior</h2>
            </header>

            <h3 id="changing-the-dictionary">Changing the dictionary</h3>
            <p>You can easily alter the function to look up your words in a different online dictionary. However, not
                all
                dictionaries are compatible--it has to be one that returns a 404 HTTP response code when you try to
                visit
                the
                page for a non-defined word.</p>
            <ol>
                <li>Get the URL of an example word entry from the dictionary website you would like to use. For example,
                    <code>https://www.dictionary.com/browse/cat</code>.
                </li>
                <li>Replace the word with <code>%s</code>, as in <code>https://www.dictionary.com/browse/%s</code></li>
                <li>On line 43 of <code>Code.gs</code>, remove just the URL
                    <code>https://www.dictionary.com/browse/%s</code>
                    and paste your modified URL in its place. Make sure it's still contained in quote marks and that the
                    rest of the line is unchanged.
                </li>
            </ol>

            <h3 id="reporting-correct-cells">Reporting correct cells</h3>
            <p>Between line 51 and line 52 (that is, right before <code>return typos</code>), add a function that checks
                the
                length of <code>typos</code>. If the length is greater than 0, it should return <code>typos</code> as
                is. If
                the
                length is 0 (array is empty), then it should either add an 'all clear' phrase to the list and return it,
                or return that phrase instead of returning the list. </p>

            <h3 id="word-delineation">Word delineation</h3>
            <p>The script uses a regular expression to define how the content of a cell is divided into words. Currently
                that
                expression is <code>/[^a-zA-Z']+/</code>. You may want to edit this regex to change what the script
                accepts
                as a word.</p>
        </section>

        <section id="Further_development" class="main-section">
            <header>
                <h2>Further development</h2>
            </header>

            <h3 id="known-issues">Known issues</h3>
            <ul>
                <li>The script will stop working if you use it on too big of a spreadsheet at once (&gt;100 lines),
                    because
                    Dictionary.com will start rejecting the HTTP requests if too many are submitted too fast.</li>
                <li>The dictionary currently used, Dictionary.com, contains some entries such as abbreviations, atomic
                    symbols,
                    etc that may not normally be considered words. </li>
                <li>The regex splits words on numerals, so words such as "1st" will come out as "st"</li>
            </ul>

            <h3 id="roadmap">Roadmap</h3>
            <p>Possible future improvements include:</p>
            <ul>
                <li>Adding a built-in dictionary so that it doesn't rely on HTTP requests</li>
            </ul>

            <h3 id="contributing">Contributing</h3>
            <p>Any improvements are welcome! Feel free to submit a <a
                    href="https://github.com/caesiumtea/sheets-spellcheck/issues">GitHub issue</a> with any suggestions
                or
                bugs
                you encounter, or to fork and submit a pull request.</p>
            <p>I'd especially love help with setting up that built-in dictionary!</p>
        </section>

        <section id="License" class="main-section">
            <header>
                <h2>License</h2>
            </header>
            <p>This code is licensed under the Hippocratic License, an <em>almost</em> open license that says you can do
                basically anything you want with this code as long as it doesn't hurt people. Check out <a
                    href="https://github.com/caesiumtea/sheets-spellcheck/blob/main/LICENSE.md">LICENSE.md</a> in the
                GitHub
                repo, as well as the <a href="https://firstdonoharm.dev/">Hippocratic License website</a>.</p>
        </section>

        <section id="Source_code" class="main-section">
            <header>
                <h2>Source code</h2>
            </header>
            <p><a href="https://github.com/caesiumtea/sheets-spellcheck">View the source code on GitHub</a>.</p>
            <pre>
/**
 * Google Sheets spell check function
 * version 1.1, 8/17/22
 * caesiumtea
 */

/**
 * Shows misspelled words in input cell or cells.
 *
 * @param {string|Array&lt;Array&lt;string&gt;&gt;} input The cell or range of cells
 *     to check spelling in.
 * @return String listing any non-dictionary words in the input.
 * @customfunction
 */
function SPCHECK(input) {
  if (Array.isArray(input)) {
    return input.map(row => row.map(cell => hasTypo_(cell) ))
  }
  else {
    return hasTypo_(input);
  }   
}

/**
 * Helper function (not accessible from Sheets):
 *   Splits the input cell into a list of words.
 *   Decides whether each word is spelled correctly by checking
 *   whether it has a page on dictionary.com.
 * Returns a string containing any unlisted words; 
 *   empty string if every word in the cell was in the dictionary.
 */
function hasTypo_(cell) {
  let typoFound = false;
  let url;
  let lookup;
  let typos = "";

  let words = cell.split(/[^a-zA-Z']+/); 
    // regex: split on anything besides letters and apostrophes

  // for word in words, if word is not in dictionary, add to typos
  for (let w of words) {
    url = Utilities.formatString("https://www.dictionary.com/browse/%s", w);
      //checks whether the word has a page on dictionary.com
    lookup = UrlFetchApp.fetch(url, {'muteHttpExceptions': true});
    if (lookup.getResponseCode() == 404) {
      // this means the word doesn't exist in the dictionary
      typoFound = true;
      typos += w + " ";
    }
  }
  return typos;
}
    </pre>

        </section>
    </main>
    <footer>
        <p>By <a href="https://caesiumtea.glitch.me/">caesiumtea</a> for <a
            href="https://www.freecodecamp.org/">freeCodeCamp</a></p>
        <p>Source code available on <a href="https://gitlab.com/caesiumtea/techdoc">GitLab</a>, semi-open under the <a
            href="https://firstdonoharm.dev/">Hippocratic License</a></p>
</footer>
</body>

</html>