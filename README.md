# Technical Documentation Project: Documentation for Google Sheets Spellcheck Function

Documentation page detailing usage and design of a [Google Apps Script function](https://github.com/caesiumtea/sheets-spellcheck) I wrote that adds passive spell-checking to Google Sheets. This website was created for the project "Technical Documentation Page" from the [Responsive Web Design certification on freeCodeCamp](https://www.freecodecamp.org/learn/2022/responsive-web-design/). That said, the text content is my own and this is a genuine attempt to document the script that I wrote.

[View live site on GitLab Pages](https://caesiumtea.gitlab.io/techdoc/).

Any feedback would be appreciated! (on the website, or even on the script itself!)

Development time: ~4 hours

## Learning
### Skills used
- Documentation writing
- Responsive design with media queries
- CSS variables
- CSS grid
- Sticky positioning
- Separation of concerns
  - Using h2 elements for section headings even when making their text appear larger than the h1
  
### Lessons learned
- When trying to make a grid column stay in a fixed position on the screen, it's important to use `sticky` instead of `fixed` so that it's not removed from the grid layout. Also, it won't stay stuck unless you add a value to a side like `top: 0`!
- Long documents with many sections are tedious to write in HTML... It speeds things up a lot to write the content in Markdown first and then convert it to HTML, to get the main structure of the page, and then go back in to add the semantic elements.
- Learned a ton about how documentation is written and organized! No major insights aside from what's reflected in the page itself though.
- Stumbled on a neat little trick: For navigation links organized as lists, if you apply a bottom border to the `a` element and then apply the same border style to the `hover` state of the `li` element, it creates the effect of an underline that stretches out when hovered. One thing to watch out for though: if you have `box-sizing` set to `border-box`, then having a border only on the hovered state will make your other elements move around when links are hovered, because it's creating a border that didn't exist before! One solution is to set a transparent border of the same size on the default, non-hovered state. It would probably be even cooler if you used CSS animation to make the line grow out gradually!
- The way to prevent paragraph line breaks from falling within an inline element is to set `white-space: nowrap`.

## License
The Hippocratic License is an *almost* open license that says you can do basically anything you want with this code as long as it doesn't hurt people. Check out [LICENSE.md](LICENSE.md) as well as the [Hippocratic License website](https://firstdonoharm.dev/).

## Acknowledgements
- Thanks to [Bad Website Club](https://badwebsite.club/) for teaching me this stuff! :D

## Author
Hey, I'm **caesiumtea**, AKA Vance! Feel free to contact me with any feedback.
- [Website and social links](https://caesiumtea.glitch.me/)
- [@caesiumtea_dev on Twitter](https://www.twitter.com/caesiumtea_dev)
- [@entropy@mastodon.social](https://mastodon.social/@entropy)
